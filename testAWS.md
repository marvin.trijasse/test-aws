# AWS test

## Subject: New platform build on AWS

## Scenario

You are challenged with building a new entire platform on AWS. 
The platform will serve as the foundation for a highly available, scalable, and secure application. Components are developped as micro-services to be deployed inside of a kubernetes cluster. 
 The Cluster treat communication with external devices at a ratio of 1M requests per minute. The following aspects are critical to the success of this project:

## Whats needed

- Access and Service Account Management

You need to design a comprehensive approach to managing user access, roles, and service accounts to ensure secure access to AWS resources.

- AWS Services

Select and justify the AWS services you would utilize to build this platform. Consider factors like compute, storage, databases, networking, and any other services that you find essential. There is no limitation in which services you can use as long as you can justify their usage

- Scalability

The platform should be designed to handle rapid growth in user traffic and data. Describe how you would architect the platform for scalability, including strategies or design pattern for scaling in term of compute capacity or networking capacity.

- Internal and External Networking

You must establish both internal and external networking. Secure and well-connected exposure to the outside of your plateform aswell as reliable communication between your different services.

## Instructions

- Please provide a high-level architecture of the platform, focusing on the points mentioned above. You can use diagrams or text-based descriptions to illustrate your design.
- Elaborate on the access and service account management strategy. [explaining how you would ensure the least privilege principle and robust access controls within AWS.]
- List and justify the AWS services you would employ in building this platform. Explain how each service contributes to the overall architecture.
- Describe the mechanisms you would employ for scalability. Include specific AWS services or features or non-AWS services that you would use (You may link the documentation for such third-party tools along with your justification). [auto-scaling and load balancing.]
- Provide a detailed explanation of how you would set up internal and external networking within AWS. [Include VPC design, subnet configurations, and security measures].


Optionally, if you have any additional insights or experiences related to similar projects, feel free to share them.

## Evaluation Criteria

The candidate's response will be assessed based on:

- Clarity and completeness of the architectural design.
- Relevance of architectural choices.
- Ability to describe and communicate about your different choices.

This exercise will not only help you evaluate the candidate's technical expertise but also their ability to design and communicate a comprehensive and pertinent AWS infrastructure. It's essential to assess their ability to address the critical aspects outlined in the exercise.
